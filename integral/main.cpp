#include <iostream>
using namespace std;
double integral_mc(double, double, int);


int main() {

    //Wczytanie a i b + sprawdzenie, czy a>b, jak nie koniec programu lub wczytanie ponownie
    
    cout << "Podaj wartosc a: " << endl;
    double a;
    cin >> a;

    cout << "Podaj wartosc b: " << endl;
    double b;
    cin >>b;
    
    while (a <= b) {
        cout << "a musi byc wieksze od b." << endl;
        cout << "b wynosi: " << b << endl;
        cout << "Podaj wartosc a: " << endl;
        cin >> a;

    }

    //Wczytanie n, czy jest poprawne, integer
    cout << "Podaj wartosc n: " << endl;
    int n;
    cin >> n;
    while (n <= 0) {
        cout << "n musib byc wieksze od 0. " << endl;
        cout << "Podaj wartosc n: " << endl;
        cin >> n;
    }


    // Wywolanie funkcji integral_mc i wypisane wyniku
    cout << "Wartosc calki: " << integral_mc(a, b, n) << endl;
}

double f(double x) {
    return x * x + 5 * x - 3;
}

double integral_mc(double a, double b, int n) {

    srand(time(NULL));
    int i = 1;
    double sum = 0;
    while ( i <= n ) {
        //losowa liczba zmiennoprzecinkowa, z przedzialu [a,b]

        double xlos = (a + (double)rand()) / (double)RAND_MAX * (b - a);
        sum = sum + f(xlos);
        i++;
    }

    return (b - a) / n * sum;
}



